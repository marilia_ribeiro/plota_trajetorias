$(document).ready(function () {
    
    var map_html_object =  document.getElementById('map');
    
    var desenhar = false;
    var coordenadas_desenho = Array();
    var trajetoria_desenhada = null;
    
    var trajectory_config = {
        color: '#00ff00',
        weight: 3.0,
        opacity: 1.0
    }

    // Inicialização do mapa
    var initMap = function (center_point, map_html_object) {
        var map = new google.maps.Map(map_html_object, {
            center: center_point,
            scrollwheel: true,
            zoom: 16
        });
        
        google.maps.event.addListener(map, 'click', function (e) {
            if(desenhar == true)
            {
                lat = parseFloat(e.latLng.lat().toFixed(6));
                lng = parseFloat(e.latLng.lng().toFixed(6));
                
                ponto = {lat: lat, lng: lng};
                coordenadas_desenho.push(ponto);
                plotarTrajetoriaPrevia();
            }
            else
            {
                alert('Para desenhar ative o botão "Desenhar"');
            }
        });

        return map;
    };

    var plotarTrajetoriaPrevia = function () {
        if(trajetoria_desenhada != null)
            trajetoria_desenhada.setMap(null);

        var trajetoria = new google.maps.Polyline({
            path: coordenadas_desenho,
            geodesic: true,
            strokeColor: '#ff0000',
            strokeWeight: 2,
            map: map
        });
        trajetoria_desenhada = trajetoria;
    }

    var setCenterPoint = function (lat, lng) {
        var center_point = {
            lat: lat, 
            lng: lng
        };
        return center_point;
    }

    $('#inserir_trajetoria').click(function (){
        if(desenhar == false)
        {
            nome = $('#nome_trajetoria').val();
            coordenadas = $('#coordenadas_trajetoria').val();
            cor = $('#cor_trajetoria').val();
            destaque = $('#destacar_trajetoria').is(':checked');

            pontos_da_trajetoria = converteTextoParaCoordenadas(coordenadas);
            trajetoria = plotaTrajetoria(nome, cor, pontos_da_trajetoria, destaque);
            trajetorias_registradas.push(trajetoria);
            listarTrajetoria(trajetoria);
        }
        else
        {
            alert("Termine o desenho para continuar!");
        }
    });

    $('#inserir_trajetoria_original').click(function (){
            pontos_t1 = converteTextoParaCoordenadas('{"x": -26.3367132, "y": -48.8323402}, {"x": -26.3364079, "y": -48.8317582}, {"x": -26.3366579, "y": -48.8316402}, {"x": -26.33673, "y": -48.8315302}, {"x": -26.3367132, "y": -48.8314149}, {"x": -26.3366435, "y": -48.8313344}, {"x": -26.3364223, "y": -48.8312083}, {"x": -26.3361627, "y": -48.8310689}, {"x": -26.3356819, "y": -48.8309348}, {"x": -26.335105, "y": -48.8309991}, {"x": -26.3342492, "y": -48.8310742}, {"x": -26.33423, "y": -48.8304949}');
            trajetoria = plotaTrajetoria("t1", "#000000", pontos_t1, false);
            trajetorias_registradas.push(trajetoria);
            listarTrajetoria(trajetoria);
    });

    $('#desenhar').click(function () {
        if(desenhar == false){
            desenhar = true;
            $(this).val("Parar de desenhar");
            $('#apagar_desenho').removeClass("disabled");
            $('#salvar_desenho').removeClass("disabled");
        }
        else{
            desenhar = false;
            $(this).val("Desenhar");   
            $('#apagar_desenho').addClass("disabled");
            $('#salvar_desenho').addClass("disabled");
        }
    });

    $('#apagar_desenho').click(function () {
        apagarDesenho();
    });

    $('#salvar_desenho').click(function () {
        trajetoria = plotaTrajetoria('Trajetória Desenhada', '#000000', coordenadas_desenho);
        trajetorias_registradas.push(trajetoria);
        listarTrajetoria(trajetoria);
        apagarDesenho();
    });

    var apagarDesenho = function () {
        coordenadas_desenho = Array();
        if(trajetoria_desenhada != null)
            trajetoria_desenhada.setMap(null);
        trajetoria_desenhada = null;
    }

    var plotaTrajetoria = function (nome, cor, coordenadas, destaque) {

        if(cor=="") cor = "#000000";

        weight = 2;
        opacity = 0.4;
        if(destaque){
            weight = 3;
            opacity = 1.0;
        }

        var trajetoria = new google.maps.Polyline({
            path: coordenadas,
            geodesic: true,
            strokeColor: cor,
            strokeWeight: weight,
            strokeOpacity: opacity,
            map: map
        });

        if(cor != "#000000")
            var trajetoria = new google.maps.Polyline({
                path: coordenadas,
                geodesic: true,
                strokeColor: cor,
                strokeWeight: 4,
                map: map
            });
        if(destaque)
            plotaPontosDaTrajetoria(nome, coordenadas);
        return {pos: trajetorias_registradas.length, trj: trajetoria, nome: nome, cor: cor};
    };

    var plotaPontosDaTrajetoria = function (nome, pontos) {
        for(var p in pontos){
            ponto = pontos[p];
            var marcador = new google.maps.Marker({
                position: ponto,
                map: map,
                title: nome+': '+ponto.lat.toString()+', '+ponto.lng.toString()
            });
        }

        //alert(nome);
    }

    var listarTrajetoria = function (trajetoria) {
        lista = $('#lista_trajetorias');
        item = '<div class="item" style="border-left: 4px solid '+trajetoria.cor+'!important;">\
            <span class="header">'+trajetoria.nome+'</span>\
            <a>Retornar coordenadas (Não funcional)</a> \
        </div>';
        lista.html(lista.html() + item);
    }

    var converteTextoParaCoordenadas = function (coordenadas) {
        /* {"y": -48.612953, "x": -26.229400000000002}, {"y": -48.610202, "x": -26.230796000000005}, {"y": -48.60882899999999, "x": -26.23096} */
        var pontos = Array();
        var pontos_separados = coordenadas.split("},");
        for(var i in pontos_separados){
            coordenadas_do_ponto = pontos_separados[i].split(",");
            ponto = construirPonto(coordenadas_do_ponto);
            pontos.push(ponto);
        }
        return pontos;
    };


    var construirPonto = function (coordenadas_do_ponto) {
        if(coordenadas_do_ponto[0].indexOf('"x":') != -1){
            latitude = identificaLatitude(coordenadas_do_ponto[0]);
            longitude = identificaLongitude(coordenadas_do_ponto[1]);
        }
        else{
            latitude = identificaLatitude(coordenadas_do_ponto[1]);
            longitude = identificaLongitude(coordenadas_do_ponto[0]);
        }

        return {lat: parseFloat(latitude), lng: parseFloat(longitude)};
    };

    var identificaLongitude = function(string) {
        inicio_do_valor = string.indexOf('y') + 3;
        final_do_valor = string.indexOf('.') + 6;
        valor = string.substr(inicio_do_valor, final_do_valor);
        valor = parseFloat(valor).toFixed(6);
        return valor;
    };

    var identificaLatitude = function(string) {
        inicio_do_valor = string.indexOf('x') + 3;
        final_do_valor = string.indexOf('.') + 6;
        valor = string.substr(inicio_do_valor, final_do_valor);
        valor = valor.replace('}', '');
        valor = parseFloat(valor).toFixed(6);
        return valor;
    };

    //var center_points = setCenterPoint(latitude, longitude);      // modelo
    //var center_point = setCenterPoint(-26.297722, -48.840411);      // Joinville
    //var center_point = setCenterPoint(-26.230796,-48.610202);    // São Francisco do Sul
    //var center_point = setCenterPoint(-22.915899, -43.220478);    // Rio de Janeiro
    var center_point = setCenterPoint(-26.335470, -48.831286);      // Itaum
    
    var map = initMap(center_point, map_html_object);
    var trajetorias_registradas = Array();

    
})
